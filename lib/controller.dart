import 'dart:async';

import 'package:player/common/controller.dart';
import 'package:video_player/video_player.dart';

class PlayerController extends IPlayerController<VideoPlayerController?> {
  VideoPlayerController? _controller;

  PlayerController(String? initLink,
      {Duration? initDuration, Map<String, String> httpHeaders = const <String, String>{}})
      : super(initLink: initLink, initDuration: initDuration, httpHeaders: httpHeaders);

  @override
  VideoPlayerController? get baseController => _controller;

  @override
  bool isPlaying() {
    if (_controller == null) {
      return false;
    }

    return _controller!.value.isPlaying;
  }

  @override
  Duration position() {
    if (_controller == null) {
      return const Duration();
    }
    return _controller!.value.position;
  }

  @override
  double aspectRatio() {
    if (_controller == null) {
      return 16 / 9;
    }

    return _controller!.value.aspectRatio;
  }

  @override
  Future<void> pause() async {
    if (_controller == null) {
      return Future.error('Invalid state');
    }

    return _controller!.pause();
  }

  @override
  Future<void> play() async {
    if (_controller == null) {
      return Future.error('Invalid state');
    }

    return _controller!.play();
  }

  @override
  Future<void> seekTo([Duration duration = const Duration(seconds: 5)]) async {
    if (_controller == null) {
      return Future.error('Invalid state');
    }

    return _controller!.seekTo(duration);
  }

  @override
  Future<void> setPlaybackSpeed(double speed) async {
    if (_controller == null) {
      return Future.error('Invalid state');
    }

    if (speed <= 0) {
      return Future.error('Invalid speed');
    }

    return _controller!.setPlaybackSpeed(speed);
  }

  @override
  Future<void> setVolume(double volume) {
    if (_controller == null) {
      return Future.error('Invalid state');
    }
    return _controller!.setVolume(volume);
  }

  @override
  Future setStreamUrl(String? url) async {
    if (url == null) {
      return Future.error('Invalid input');
    }

    final VideoPlayerController? old = _controller;
    _controller = VideoPlayerController.network(url,
        httpHeaders: httpHeaders, videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true));

    old?.pause();
    final resp = _controller!.initialize();
    final result = resp.then((_) {
      old?.dispose();
    }, onError: (error) {
      old?.dispose();
    });
    return result;
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }
}
